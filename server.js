// Use Express
var express = require("express");
// Use body-parser
var bodyParser = require("body-parser");
// Use MongoDB
var mongodb = require("mongodb");
var ObjectID = mongodb.ObjectID;
// The database variable
var database;
// The employees collection
var EMPLOYEES_COLLECTION = "employees";
var OFFICES_COLLECTION = "offices";
var TAGS_COLLECTION = "tags";
var USERS_COLLECTION = "users";

// Create new instance of the express server
var app = express();

// Define the JSON parser as a default way 
// to consume and produce data through the 
// exposed APIs
app.use(bodyParser.json());

// Create link to Angular build directory
// The `ng build` command will save the result
// under the `dist` folder.
var distDir = __dirname + "/dist/";
app.use(express.static(distDir));
// Local database URI.
const LOCAL_DATABASE = "mongodb://localhost:27017/app";
// Local port.
const LOCAL_PORT = 8080;

// Init the server
mongodb.MongoClient.connect(process.env.MONGODB_URI || LOCAL_DATABASE,
    {
        useUnifiedTopology: true,
        useNewUrlParser: true,
    }, function (error, client) {

        // Check if there are any problems with the connection to MongoDB database.
        if (error) {
            console.log(error);
            process.exit(1);
        }

        // Save database object from the callback for reuse.
        database = client.db();
        console.log("Database connection done.");

        // Initialize the app.
        var server = app.listen(process.env.PORT || LOCAL_PORT, function () {
            var port = server.address().port;
            console.log("App now running on port", port);
        });
    });


//AUTH
app.post("/api/login", function (req, res) {
    var login = req.body;

    if (!login.username) {
        manageError(res, "Invalid employee input", "Username is mandatory.", 400);
    }if (!login.password) {
        manageError(res, "Invalid employee input", "Password is mandatory.", 400);
    } else {
        if(login.username == "admin" && login.password == "admin"){
            res.status(200).json({
                status: "OK"
            });
        }
        else{
            manageError(res, "Username or password is incorrect.", "Username or password is incorrect.", 401);
        }
    }
});

app.get("/api/status", function (req, res) {
    res.status(200).json({ status: "UP" });
});


//EMPLOYEES
app.get("/api/employees", function (req, res) {
    database.collection(EMPLOYEES_COLLECTION).aggregate([
        { 
            $lookup: {
                from: 'offices',
                localField: 'office_id',
                foreignField: '_id',
                as: 'office'
            }
        }
    ]).toArray(function (error, data) {
        if (error) {
            manageError(res, err.message, "Failed to get employees.");
        } else {
            res.status(200).json(data);
        }
    });
});

app.post("/api/employee", function (req, res) {
    var employee = req.body;

    if (!employee.firstName) {
        manageError(res, "Invalid employee input", "First Name is mandatory.", 400);
    }if (!employee.lastName) {
        manageError(res, "Invalid employee input", "Last Name is mandatory.", 400);
    }if (!employee.birthDate) {
        manageError(res, "Invalid employee input", "Birth date is mandatory.", 400);
    }if (!employee.phoneNumber) {
        manageError(res, "Invalid employee input", "Phone number is mandatory.", 400);
    }if (!employee.officePlace) {
        manageError(res, "Invalid employee input", "Office place is mandatory.", 400);
    }if (!employee.tags) {
        manageError(res, "Invalid employee input", "Tags is mandatory.", 400);
    } else {
        employee.office_id = new ObjectID(employee.office_id)
        database.collection(EMPLOYEES_COLLECTION).insertOne(employee, function (err, doc) {
            if (err) {
                manageError(res, err.message, "Failed to create new employee.");
            } else {
                res.status(201).json(doc.ops[0]);
            }
        });
    }
});

app.put("/api/employee/:id", function (req, res) {
    var employee = req.body;

    if (req.params.id.length > 24 || req.params.id.length < 24) {
        manageError(res, "Invalid employee id", "ID must be a single String of 12 bytes or a string of 24 hex characters.", 400);
    } else {
        employee.office_id = new ObjectID(employee.office_id)
        database.collection(EMPLOYEES_COLLECTION).updateOne({ _id: new ObjectID(req.params.id) }, {$set: employee}, function (err, data) {
            if (err) {
                manageError(res, err.message, "Failed to update employee.");
            } else {
                res.status(200).json(req.params.id);
            }
        });
    }
});

app.get("/api/employee/:id", function (req, res) {
    if (req.params.id.length > 24 || req.params.id.length < 24) {
        manageError(res, "Invalid employee id", "ID must be a single String of 12 bytes or a string of 24 hex characters.", 400);
    } else {
        database.collection(EMPLOYEES_COLLECTION).findOne({ _id: new ObjectID(req.params.id) }, function (err, data) {
            if (err) {
                manageError(res, err.message, "Failed to delete employee.");
            } else {
                res.status(200).json(data);
            }
        });
    }
});

app.delete("/api/employee/:id", function (req, res) {
    if (req.params.id.length > 24 || req.params.id.length < 24) {
        manageError(res, "Invalid employee id", "ID must be a single String of 12 bytes or a string of 24 hex characters.", 400);
    } else {
        database.collection(EMPLOYEES_COLLECTION).deleteOne({ _id: new ObjectID(req.params.id) }, function (err, result) {
            if (err) {
                manageError(res, err.message, "Failed to delete employee.");
            } else {
                res.status(200).json(req.params.id);
            }
        });
    }
});

//OFFICES
app.get("/api/offices", function (req, res) {
    database.collection(OFFICES_COLLECTION).find({}).toArray(function (error, data) {
        if (error) {
            manageError(res, err.message, "Failed to get OFFICEs.");
        } else {
            res.status(200).json(data);
        }
    });
});

app.post("/api/office", function (req, res) {
    var office = req.body;

    if (!office.name) {
        manageError(res, "Invalid office input", "Name is mandatory.", 400);
    } else {
        database.collection(OFFICES_COLLECTION).insertOne(office, function (err, doc) {
            if (err) {
                manageError(res, err.message, "Failed to create new office.");
            } else {
                res.status(201).json(doc.ops[0]);
            }
        });
    }
});

app.put("/api/office/:id", function (req, res) {
    var office = req.body;

    if (req.params.id.length > 24 || req.params.id.length < 24) {
        manageError(res, "Invalid office id", "ID must be a single String of 12 bytes or a string of 24 hex characters.", 400);
    } else {
        database.collection(OFFICES_COLLECTION).updateOne({ _id: new ObjectID(req.params.id) }, {$set: office}, function (err, data) {
            if (err) {
                manageError(res, err.message, "Failed to update office.");
            } else {
                res.status(200).json(req.params.id);
            }
        });
    }
});

app.get("/api/office/:id", function (req, res) {
    if (req.params.id.length > 24 || req.params.id.length < 24) {
        manageError(res, "Invalid office id", "ID must be a single String of 12 bytes or a string of 24 hex characters.", 400);
    } else {
        database.collection(OFFICES_COLLECTION).findOne({ _id: new ObjectID(req.params.id) }, function (err, data) {
            if (err) {
                manageError(res, err.message, "Failed to delete office.");
            } else {
                res.status(200).json(data);
            }
        });
    }
});

app.delete("/api/office/:id", function (req, res) {
    if (req.params.id.length > 24 || req.params.id.length < 24) {
        manageError(res, "Invalid office id", "ID must be a single String of 12 bytes or a string of 24 hex characters.", 400);
    } else {
        database.collection(OFFICES_COLLECTION).deleteOne({ _id: new ObjectID(req.params.id) }, function (err, result) {
            if (err) {
                manageError(res, err.message, "Failed to delete office.");
            } else {
                res.status(200).json(req.params.id);
            }
        });
    }
});

//TAGS
app.get("/api/tags", function (req, res) {
    database.collection(TAGS_COLLECTION).find({}).toArray(function (error, data) {
        if (error) {
            manageError(res, err.message, "Failed to get TAGs.");
        } else {
            res.status(200).json(data);
        }
    });
});

app.post("/api/tag", function (req, res) {
    var tag = req.body;

    if (!tag.name) {
        manageError(res, "Invalid tag input", "Name is mandatory.", 400);
    } else {
        database.collection(TAGS_COLLECTION).insertOne(tag, function (err, doc) {
            if (err) {
                manageError(res, err.message, "Failed to create new tag.");
            } else {
                res.status(201).json(doc.ops[0]);
            }
        });
    }
});

app.put("/api/tag/:id", function (req, res) {
    var tag = req.body;

    if (req.params.id.length > 24 || req.params.id.length < 24) {
        manageError(res, "Invalid tag id", "ID must be a single String of 12 bytes or a string of 24 hex characters.", 400);
    } else {
        database.collection(TAGS_COLLECTION).updateOne({ _id: new ObjectID(req.params.id) }, {$set: tag}, function (err, data) {
            if (err) {
                manageError(res, err.message, "Failed to update tag.");
            } else {
                res.status(200).json(req.params.id);
            }
        });
    }
});

app.get("/api/tag/:id", function (req, res) {
    if (req.params.id.length > 24 || req.params.id.length < 24) {
        manageError(res, "Invalid tag id", "ID must be a single String of 12 bytes or a string of 24 hex characters.", 400);
    } else {
        database.collection(TAGS_COLLECTION).findOne({ _id: new ObjectID(req.params.id) }, function (err, data) {
            if (err) {
                manageError(res, err.message, "Failed to delete tag.");
            } else {
                res.status(200).json(data);
            }
        });
    }
});

app.delete("/api/tag/:id", function (req, res) {
    if (req.params.id.length > 24 || req.params.id.length < 24) {
        manageError(res, "Invalid tag id", "ID must be a single String of 12 bytes or a string of 24 hex characters.", 400);
    } else {
        database.collection(TAGS_COLLECTION).deleteOne({ _id: new ObjectID(req.params.id) }, function (err, result) {
            if (err) {
                manageError(res, err.message, "Failed to delete tag.");
            } else {
                res.status(200).json(req.params.id);
            }
        });
    }
});


// Errors handler.
function manageError(res, reason, message, code) {
    console.log("Error: " + reason);
    res.status(code || 500).json({ "error": message });
}
