import { Tag } from './tag';
import { Office } from './office';

export interface Employee {
    _id?: string,
    firstName: string,
    lastName: string,
    officePlace: Office,
    birthDate: string,
    phoneNumber: string,
    tags: Tag[]
}
