import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './layouts/main/main.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ListComponent as EmployeesListComponent } from './pages/employees/list/list.component';
import { AddComponent as EmployeesAddComponent} from './pages/employees/add/add.component';
import { EditComponent as EmployeesEditComponent} from './pages/employees/edit/edit.component';
import { ListComponent as OfficeListComponent} from './pages/offices/list/list.component';
import { AddComponent as OfficeAddComponent } from './pages/offices/add/add.component';
import { EditComponent as OfficeEditComponent } from './pages/offices/edit/edit.component';
import { ListComponent as TagListComponent } from './pages/tags/list/list.component';
import { AddComponent as TagAddComponent } from './pages/tags/add/add.component';
import { EditComponent as TagEditComponent } from './pages/tags/edit/edit.component';
import { AuthComponent } from './layouts/auth/auth.component';
import { LoginComponent } from './pages/login/login.component';
import { IsAuthGuard } from './guards/is-auth.guard';
import { IsNotAuthGuard } from './guards/is-not-auth.guard';

const routes: Routes = [
  {path: '', component: AuthComponent, canActivate: [IsNotAuthGuard], children: [
    {path: '', redirectTo: "login", pathMatch: "full"},
    {path: 'login', component: LoginComponent}
  ]},

  {path: '', component: MainComponent, canActivate: [IsAuthGuard], children: [
    {path: '', redirectTo: "dashboard", pathMatch: "full"},
    {path: 'dashboard', component: DashboardComponent},
    {path: 'employees', component: EmployeesListComponent},
    {path: 'employee/add', component: EmployeesAddComponent},
    {path: 'employee/edit/:id', component: EmployeesEditComponent},
    {path: 'offices', component: OfficeListComponent},
    {path: 'office/add', component: OfficeAddComponent},
    {path: 'office/edit/:id', component: OfficeEditComponent},
    {path: 'tags', component: TagListComponent},
    {path: 'tag/add', component: TagAddComponent},
    {path: 'tag/edit/:id', component: TagEditComponent}
  ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
