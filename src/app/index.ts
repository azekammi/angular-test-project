import { MainComponent } from "./layouts/main/main.component";
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { HeaderComponent } from './components/header/header.component';
import { ListComponent as EmployeesListComponent } from './pages/employees/list/list.component';
import { AddComponent as EmployeesAddComponent} from './pages/employees/add/add.component';
import { EditComponent as EmployeesEditComponent} from './pages/employees/edit/edit.component';
import { ListComponent as OfficeListComponent} from './pages/offices/list/list.component';
import { AddComponent as OfficeAddComponent } from './pages/offices/add/add.component';
import { EditComponent as OfficeEditComponent } from './pages/offices/edit/edit.component';
import { ListComponent as TagListComponent} from './pages/tags/list/list.component';
import { EditComponent as TagEditComponent } from './pages/tags/edit/edit.component';
import { AddComponent as TagAddComponent } from './pages/tags/add/add.component';
import { DialogComponent } from './modals/dialog/dialog.component';
import { LoaderComponent } from './components/loader/loader.component';
import { AuthComponent } from './layouts/auth/auth.component';
import { LoginComponent } from './pages/login/login.component';

export const LAYOUTS = [
  MainComponent,
  AuthComponent
];

export const PAGES = [
  DashboardComponent,
  EmployeesListComponent,
  EmployeesAddComponent,
  EmployeesEditComponent,
  OfficeListComponent,
  OfficeAddComponent,
  OfficeEditComponent,
  TagListComponent,
  TagEditComponent,
  TagAddComponent,
  LoginComponent
]

export const COMPONENTS = [
  HeaderComponent,
  LoaderComponent
]

export const MODALS = [
  DialogComponent
]