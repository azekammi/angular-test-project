import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  loaderState: BehaviorSubject<number> = new BehaviorSubject<number>(0)

  appComponent: any

  constructor() { }

  increaseLoader(){
    this.loaderState.next(this.loaderState.value + 1)
  }

  decreaseLoader(){
    this.loaderState.next(this.loaderState.value - 1)
  }


}
