import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Office } from '../interfaces/office';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OfficeService {

  constructor(
    private http: HttpClient
  ) { }

  getOffices(){
    return this.http.get<Office[]>(`${environment.BASE_API_URL}/offices`)
  }

  getOffice(id: string){
    return this.http.get<Office>(`${environment.BASE_API_URL}/office/${id}`)
  }

  addOffice(data: Office){
    return this.http.post<Office>(`${environment.BASE_API_URL}/office`, data)
  }

  updateOffice(id: string, data: Office){
    return this.http.put<string>(`${environment.BASE_API_URL}/office/${id}`, data)
  }

  removeOffice(id: string){
    return this.http.delete<string>(`${environment.BASE_API_URL}/office/${id}`)
  }
}
