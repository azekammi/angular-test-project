import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Tag } from '../interfaces/tag';

@Injectable({
  providedIn: 'root'
})
export class TagService {

  constructor(
    private http: HttpClient
  ) { }

  getTags(){
    return this.http.get<Tag[]>(`${environment.BASE_API_URL}/tags`)
  }

  getTag(id: string){
    return this.http.get<Tag>(`${environment.BASE_API_URL}/tag/${id}`)
  }

  addTag(data: Tag){
    return this.http.post<Tag>(`${environment.BASE_API_URL}/tag`, data)
  }

  updateTag(id: string, data: Tag){
    return this.http.put<string>(`${environment.BASE_API_URL}/tag/${id}`, data)
  }

  removeTag(id: string){
    return this.http.delete<string>(`${environment.BASE_API_URL}/tag/${id}`)
  }
}
