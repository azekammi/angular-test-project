import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Employee } from '../interfaces/employee';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(
    private http: HttpClient
  ) { }

  getEmployees(){
    return this.http.get(`${environment.BASE_API_URL}/employees`)
  }

  getEmployee(id: string){
    return this.http.get<any>(`${environment.BASE_API_URL}/employee/${id}`)
  }

  addEmployee(data: Employee){
    return this.http.post<Employee>(`${environment.BASE_API_URL}/employee`, data)
  }

  updateEmployee(id: string, data: Employee){
    return this.http.put<string>(`${environment.BASE_API_URL}/employee/${id}`, data)
  }

  removeEmployee(id: string){
    return this.http.delete<string>(`${environment.BASE_API_URL}/employee/${id}`)
  }
}
