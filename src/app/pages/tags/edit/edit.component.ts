import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Tag } from 'src/app/interfaces/tag';
import { TagService } from 'src/app/services/tag.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SettingsService } from 'src/app/services/settings.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.sass']
})
export class EditComponent implements OnInit {

  formGroup!: FormGroup

  _id: string|null = null

  tag: Tag|null = null

  constructor(
    private formBuilder: FormBuilder,
    private tagService: TagService,
    private router: Router,
    private route: ActivatedRoute,
    private settingsService: SettingsService
  ) { }

  ngOnInit(): void {
    this.createFormGroup()

    this.route.params
      .subscribe((params) => {
        if(params.id) {
          this._id = params.id
          this.getTag(params.id)
        }
      })
  }

  createFormGroup(){
    this.formGroup = this.formBuilder.group({
      name: new FormControl('', [Validators.required])
    })
  }

  getTag(id: string){
    this.settingsService.increaseLoader()
    this.tagService.getTag(id)
      .subscribe((response: Tag) => {
        this.settingsService.decreaseLoader()
        this.tag = response
      })
  }

  onUpdateClick(){
    if(!this.formGroup.invalid){
      let data: Tag = {
        name: this.formGroup.get("name")?.value
      }

      this.settingsService.increaseLoader()
      this.tagService.updateTag(this._id!, data)
        .subscribe((response: string) => {
          this.settingsService.decreaseLoader()
          this.router.navigate(["/tags"])
        })
    }
  }
}
