import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { TagService } from 'src/app/services/tag.service';
import { SettingsService } from 'src/app/services/settings.service';
import { DialogComponent } from 'src/app/modals/dialog/dialog.component';
import { Tag } from 'src/app/interfaces/tag';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.sass']
})
export class ListComponent implements OnInit {

  displayedColumns: string[] = ['name', 'operations'];
  tags: Tag[] = [];

  constructor(
    public dialog: MatDialog,
    private tagService: TagService,
    private settingsService: SettingsService
  ) { }

  ngOnInit(): void {
    this.getTags()
  }

  onRemoveButtonClick(id: string){
    const dialogRef = this.dialog.open(DialogComponent)
    dialogRef.componentInstance.title = "Removing data"
    dialogRef.componentInstance.description = "Are you sure about removing?"
    dialogRef.componentInstance.hasCancel = true
    dialogRef.componentInstance.buttonText = "Yes"

    dialogRef.afterClosed().subscribe(result => {
      if(result) this.removeTag(id)
    });
  }

  getTags(){
    this.settingsService.increaseLoader()
    this.tagService.getTags()
      .subscribe((response: Tag[]) => {
        this.settingsService.decreaseLoader()

        this.tags = response
      })
  }

  removeTag(id: string){
    this.settingsService.increaseLoader()
    this.tagService.removeTag(id)
      .subscribe((response: string) => {
        this.settingsService.decreaseLoader()
        this.getTags()
      })
  }
}
