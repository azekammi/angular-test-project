import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { TagService } from 'src/app/services/tag.service';
import { SettingsService } from 'src/app/services/settings.service';
import { Router } from '@angular/router';
import { Tag } from 'src/app/interfaces/tag';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.sass']
})
export class AddComponent implements OnInit {

  formGroup!: FormGroup

  constructor(
    private formBuilder: FormBuilder,
    private tagService: TagService,
    private settingsService: SettingsService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.createFormGroup()
  }

  createFormGroup(){
    this.formGroup = this.formBuilder.group({
      name: new FormControl('', [Validators.required])
    })
  }

  onSaveClick(){
    if(!this.formGroup.invalid){

      let data: Tag = {
        name: this.formGroup.get("name")?.value
      }

      this.settingsService.increaseLoader()
      this.tagService.addTag(data)
        .subscribe((response: Tag) => {
          this.settingsService.decreaseLoader()
          this.router.navigate(["/tags"])
        })
    }
  }
}
