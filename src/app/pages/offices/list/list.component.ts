import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { OfficeService } from 'src/app/services/office.service';
import { DialogComponent } from 'src/app/modals/dialog/dialog.component';
import { SettingsService } from 'src/app/services/settings.service';
import { Office } from 'src/app/interfaces/office';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.sass']
})
export class ListComponent implements OnInit {

  displayedColumns: string[] = ['name', 'operations'];
  offices: Office[] = [];

  constructor(
    public dialog: MatDialog,
    private officeService: OfficeService,
    private settingsService: SettingsService
  ) { }

  ngOnInit(): void {
    this.getOffices()
  }

  onRemoveButtonClick(id: string){
    const dialogRef = this.dialog.open(DialogComponent)
    dialogRef.componentInstance.title = "Removing data"
    dialogRef.componentInstance.description = "Are you sure about removing?"
    dialogRef.componentInstance.hasCancel = true
    dialogRef.componentInstance.buttonText = "Yes"

    dialogRef.afterClosed().subscribe(result => {
      if(result) this.removeOffice(id)
    });
  }

  getOffices(){
    this.settingsService.increaseLoader()
    this.officeService.getOffices()
      .subscribe((response: Office[]) => {
        this.settingsService.decreaseLoader()

        this.offices = response
      })
  }

  removeOffice(id: string){
    this.settingsService.increaseLoader()
    this.officeService.removeOffice(id)
      .subscribe((response: string) => {
        this.settingsService.decreaseLoader()
        this.getOffices()
      })
  }
}
