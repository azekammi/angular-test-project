import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { OfficeService } from 'src/app/services/office.service';
import { Office } from 'src/app/interfaces/office';
import { Router } from '@angular/router';
import { SettingsService } from 'src/app/services/settings.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.sass']
})
export class AddComponent implements OnInit {

  formGroup!: FormGroup

  constructor(
    private formBuilder: FormBuilder,
    private officeService: OfficeService,
    private settingsService: SettingsService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.createFormGroup()
  }

  createFormGroup(){
    this.formGroup = this.formBuilder.group({
      name: new FormControl('', [Validators.required])
    })
  }

  onSaveClick(){
    if(!this.formGroup.invalid){

      let data: Office = {
        name: this.formGroup.get("name")?.value
      }

      this.settingsService.increaseLoader()
      this.officeService.addOffice(data)
        .subscribe((response: Office) => {
          this.settingsService.decreaseLoader()
          this.router.navigate(["/offices"])
        })
    }
  }
}
