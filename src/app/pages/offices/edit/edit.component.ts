import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { OfficeService } from 'src/app/services/office.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Office } from 'src/app/interfaces/office';
import { SettingsService } from 'src/app/services/settings.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.sass']
})
export class EditComponent implements OnInit {

  formGroup!: FormGroup

  _id: string|null = null

  office: Office|null = null

  constructor(
    private formBuilder: FormBuilder,
    private officeService: OfficeService,
    private router: Router,
    private route: ActivatedRoute,
    private settingsService: SettingsService
  ) { }

  ngOnInit(): void {
    this.createFormGroup()

    this.route.params
      .subscribe((params) => {
        if(params.id) {
          this._id = params.id
          this.getOffice(params.id)
        }
      })
  }

  createFormGroup(){
    this.formGroup = this.formBuilder.group({
      name: new FormControl('', [Validators.required])
    })
  }

  getOffice(id: string){
    this.settingsService.increaseLoader()
    this.officeService.getOffice(id)
      .subscribe((response: Office) => {
        this.settingsService.decreaseLoader()
        this.office = response
      })
  }

  onUpdateClick(){
    if(!this.formGroup.invalid){
      let data: Office = {
        name: this.formGroup.get("name")?.value
      }

      this.settingsService.increaseLoader()
      this.officeService.updateOffice(this._id!, data)
        .subscribe((response: string) => {
          this.settingsService.decreaseLoader()
          this.router.navigate(["/offices"])
        })
    }
  }
}
