import { Component, OnInit } from '@angular/core';

import {MatDialog} from '@angular/material/dialog';
import { DialogComponent } from 'src/app/modals/dialog/dialog.component';
import { EmployeeService } from 'src/app/services/employee.service';
import { Employee } from 'src/app/interfaces/employee';
import { SettingsService } from 'src/app/services/settings.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.sass']
})
export class ListComponent implements OnInit {

  displayedColumns: string[] = ['firstName', 'lastName', 'birthDate', "phoneNumber", "officePlace", "operations"];
  employees: Employee[] = [];

  constructor(
    public dialog: MatDialog,
    private employeeService: EmployeeService,
    private settingsService: SettingsService
  ) { }

  ngOnInit(): void {
    this.getEmployees()
  }

  onRemoveButtonClick(id: string){
    const dialogRef = this.dialog.open(DialogComponent)
    dialogRef.componentInstance.title = "Removing data"
    dialogRef.componentInstance.description = "Are you sure about removing?"
    dialogRef.componentInstance.hasCancel = true
    dialogRef.componentInstance.buttonText = "Yes"

    dialogRef.afterClosed().subscribe(result => {
      if(result) this.removeEmployee(id)
    });
  }

  getEmployees(){
    this.employeeService.getEmployees()
      .subscribe((response: any) => {
        this.employees = response
      })
  }

  removeEmployee(id: string){
    this.settingsService.increaseLoader()
    this.employeeService.removeEmployee(id)
      .subscribe((response: string) => {
        this.settingsService.decreaseLoader()
        this.getEmployees()
      })
  }

}
