import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Tag } from 'src/app/interfaces/tag';
import { Office } from 'src/app/interfaces/office';
import { EmployeeService } from 'src/app/services/employee.service';
import { TagService } from 'src/app/services/tag.service';
import { OfficeService } from 'src/app/services/office.service';
import { SettingsService } from 'src/app/services/settings.service';
import { Router } from '@angular/router';
import { Employee } from 'src/app/interfaces/employee';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.sass']
})
export class AddComponent implements OnInit {

  formGroup!: FormGroup

  offices: Office[] = []

  tags: Tag[] = []

  constructor(
    private formBuilder: FormBuilder,
    private employeeService: EmployeeService,
    private tagService: TagService,
    private officeService: OfficeService,
    private settingsService: SettingsService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.createFormGroup()
    this.getOffices()
    this.getTags()
  }

  createFormGroup(){
    this.formGroup = this.formBuilder.group({
      firstName: new FormControl('', [Validators.required]),
      lastName: new FormControl('', [Validators.required]),
      officePlace: new FormControl('', [Validators.required]),
      birthDate: new FormControl('', [Validators.required]),
      phoneNumber: new FormControl('', [Validators.required]),
      tags: new FormControl([]),
    })
  }

  getOffices(){
    this.settingsService.increaseLoader()
    this.officeService.getOffices()
      .subscribe((response: Office[]) => {
        this.settingsService.decreaseLoader()

        this.offices = response
      })
  }

  getTags(){
    this.settingsService.increaseLoader()
    this.tagService.getTags()
      .subscribe((response: Tag[]) => {
        this.settingsService.decreaseLoader()

        this.tags = response
      })
  }

  onSaveClick(){
    console.log(this.formGroup)
    if(!this.formGroup.invalid){

      let data: any = {
        firstName: this.formGroup.get("firstName")?.value,
        lastName: this.formGroup.get("lastName")?.value,
        office_id: this.formGroup.get("officePlace")?.value,
        birthDate: this.formGroup.get("birthDate")?.value,
        phoneNumber: this.formGroup.get("phoneNumber")?.value,
        tags: this.formGroup.get("tags")?.value,
      }

      this.settingsService.increaseLoader()
      this.employeeService.addEmployee(data)
        .subscribe((response: Employee) => {
          this.settingsService.decreaseLoader()
          this.router.navigate(["/employees"])
        })
    }
  }

}
