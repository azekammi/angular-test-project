import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Office } from 'src/app/interfaces/office';
import { Tag } from 'src/app/interfaces/tag';
import { EmployeeService } from 'src/app/services/employee.service';
import { TagService } from 'src/app/services/tag.service';
import { OfficeService } from 'src/app/services/office.service';
import { SettingsService } from 'src/app/services/settings.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Employee } from 'src/app/interfaces/employee';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.sass']
})
export class EditComponent implements OnInit {

  formGroup!: FormGroup

  _id: string|null = null

  offices: Office[] = []

  tags: Tag[] = []

  employee: any|null = null

  constructor(
    private formBuilder: FormBuilder,
    private employeeService: EmployeeService,
    private tagService: TagService,
    private officeService: OfficeService,
    private settingsService: SettingsService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.createFormGroup()
    this.getOffices()
    this.getTags()

    this.route.params
      .subscribe((params) => {
        if(params.id) {
          this._id = params.id
          this.getEmployee(params.id)
        }
      })
  }

  createFormGroup(){
    this.formGroup = this.formBuilder.group({
      firstName: new FormControl('', [Validators.required]),
      lastName: new FormControl('', [Validators.required]),
      officePlace: new FormControl('', [Validators.required]),
      birthDate: new FormControl('', [Validators.required]),
      phoneNumber: new FormControl('', [Validators.required]),
      tags: new FormControl([]),
    })
  }

  getEmployee(id: string){
    this.settingsService.increaseLoader()
    this.employeeService.getEmployee(id)
      .subscribe((response: any) => {
        this.settingsService.decreaseLoader()
        this.employee = response
      })
  }

  getOffices(){
    this.settingsService.increaseLoader()
    this.officeService.getOffices()
      .subscribe((response: Office[]) => {
        this.settingsService.decreaseLoader()

        this.offices = response
      })
  }

  getTags(){
    this.settingsService.increaseLoader()
    this.tagService.getTags()
      .subscribe((response: Tag[]) => {
        this.settingsService.decreaseLoader()

        this.tags = response
      })
  }

  onUpdateClick(){
    if(!this.formGroup.invalid){

      let data: any = {
        firstName: this.formGroup.get("firstName")?.value,
        lastName: this.formGroup.get("lastName")?.value,
        office_id: this.formGroup.get("officePlace")?.value,
        birthDate: this.formGroup.get("birthDate")?.value,
        phoneNumber: this.formGroup.get("phoneNumber")?.value,
        tags: this.formGroup.get("tags")?.value,
      }

      this.settingsService.increaseLoader()
      this.employeeService.updateEmployee(this._id!, data)
        .subscribe((response: string) => {
          this.settingsService.decreaseLoader()
          this.router.navigate(["/employees"])
        })
    }
  }

}
