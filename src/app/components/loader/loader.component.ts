import { Component, OnInit, HostBinding } from '@angular/core';
import { SettingsService } from 'src/app/services/settings.service';

@Component({
  selector: 'loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.sass']
})
export class LoaderComponent implements OnInit {

  @HostBinding("style.display") display: string = "none"

  constructor(
    private settingsService: SettingsService
  ) { }

  ngOnInit(): void {
    this.settingsService.loaderState
      .subscribe(value => {
        this.display = value > 0 ? "flex" : "none"
      })
  }

}
