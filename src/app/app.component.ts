import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { SettingsService } from './services/settings.service';
import { DialogComponent } from './modals/dialog/dialog.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {

  constructor(
    public dialog: MatDialog,
    private settingsService: SettingsService
  ) {
    this.settingsService.appComponent = this
  }

  openDialog(title: string, description: string){
    const dialogRef = this.dialog.open(DialogComponent)
    dialogRef.componentInstance.title = title
    dialogRef.componentInstance.description = description
  }
  
}
